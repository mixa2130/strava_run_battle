import requests
from bs4 import BeautifulSoup
from lxml import html
from strava import connection_check
import os
from dotenv import load_dotenv
from pathlib import *
from Google_Sheet import GoogleSheet
from strava import json_data_read


class StravaNicknames:
    def __init__(self, acc_login, acc_pass):
        self.acc_login = acc_login
        self.acc_password = acc_pass

        self.__authorization()

    def __authorization(self, redirect_url='https://www.strava.com/session'):
        """
        Strava authorization and session creation

        :param redirect_url: strava redirect url for the authorization pass
        :raise ConnectionError: if connection was not established or failed
        """
        login_url = 'https://www.strava.com/login'
        self.session = requests.Session()
        log_in = self.session.get(login_url)

        if log_in.status_code != 200:
            raise ConnectionError

        tree = html.fromstring(log_in.text)
        csrf_token = tree.xpath('//*[@name="csrf-token"]/@content')

        parameters = {'authenticity_token': csrf_token,
                      'email': self.acc_login,
                      'password': self.acc_password
                      }
        auth = self.session.post(redirect_url, parameters)

        if auth.status_code != 200 or not connection_check(auth.text):
            raise ConnectionError

    def get_strava_nickname_from_url(self, profile_url):
        """

        :param profile_url: strava person profile url, or nickname, or
               'Check out Полинка ✨ on Strava https://www.strava.com/athletes/42324366', or
               'Профиль пользователя Mikhail Rybkin в Strava https://www.strava.com/athletes/51546155'
        :type profile_url: basestring

        :raise ConnectionError: if connection was not established or failed

        :rtype: basestring
        :return: strava nickname
        """
        subscriber_russian_athlete = profile_url.find('Профиль пользователя ')
        if subscriber_russian_athlete != -1:
            return profile_url[subscriber_russian_athlete + 21:profile_url.find(' в Strava')]

        subscriber_foreign_athlete = profile_url.find('Check out ')
        if subscriber_foreign_athlete != -1:
            return profile_url[subscriber_foreign_athlete + 10:profile_url.find(' on Strava')]

        url_start_index = profile_url.find('https://www.strava.com/athletes/')
        if url_start_index != -1:
            # function can take not only url, but already a ready-made nickname
            url = profile_url[url_start_index:len(profile_url)]

            response = self.session.get(url)
            if response.status_code != 200:
                raise ConnectionError

            soup = BeautifulSoup(response.text, 'html.parser')
            title = soup.select_one('title').text
            return title[(title.find('| ') + 2):]
        return profile_url


if __name__ == '__main__':
    load_dotenv()
    strava_session = StravaNicknames(os.getenv('LOGIN'), os.getenv('PASSWORD'))

    sheet = GoogleSheet('БГУОР')
    nicknames = sheet.get_data('strava_nickname')

    for index, nick in enumerate(nicknames):
        nick_from_url = strava_session.get_strava_nickname_from_url(nick)

        if nick != nick_from_url:
            nicknames[index] = nick_from_url

    sheet.add_data('strava_nickname', nicknames)
    # club_data = json_data_read(Path.cwd() / 'data_files' / 'clubList.json')
    # for key in club_data.keys():
    #     sheet = GoogleSheet(club_data[key]['googleSheet'])
    #     print(club_data[key]['googleSheet'])
    #     nicknames = sheet.get_data('strava_nickname')
    #
    #     for index, nick in enumerate(nicknames):
    #         nick_from_url = strava_session.get_strava_nickname_from_url(nick)
    #
    #         if nick != nick_from_url:
    #             nicknames[index] = nick_from_url
    #
    #     sheet.add_data('strava_nickname', nicknames)

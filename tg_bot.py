import telebot
import os

from dotenv import load_dotenv
from pymongo import MongoClient
from pathlib import *
from uploadsParsing import work_excel
from uploadsParsing import working_space
from uploadsParsing import creator_excel
import traceback

load_dotenv()
bot = telebot.TeleBot(os.getenv('tg_token'))
proved_users_count = 0

client = MongoClient()
with client:
    # connect to database
    db = client.proved_users

    # fetch series collection
    users = db.users
    # get last id value
    for men in users.find():
        if men:
            proved_users_count = int(men.get('_id'))


@bot.message_handler(commands=['start'])
def start_handler(message):
    msg = bot.send_message(message.chat.id, 'Привет, жду твой код авторизации')
    bot.register_next_step_handler(msg, auth)


def auth(message):
    global proved_users_count
    auth_code = message.text
    user_id = message.from_user.id
    username = message.from_user.first_name

    check = users.find_one({
        'user_id': user_id,
        'username': username
    })

    if check:
        msg = bot.send_message(message.chat.id, 'Жду файл:')
        bot.register_next_step_handler(msg, uploads_parsing)
    else:
        if auth_code == 'runderground_2020':
            proved_users_count += 1
            users.insert_one({
                '_id': proved_users_count,
                'user_id': user_id,
                'username': username
            })
            msg = bot.send_message(message.chat.id, 'Авторизация пройдена. Пожалуйста отправьте файл:')
            bot.register_next_step_handler(msg, uploads_parsing)

        else:
            msg = bot.reply_to(message, 'Неправильный код((. Попробуйте ещё разок')
            bot.register_next_step_handler(msg, auth)


@bot.message_handler(content_types=['document'], )
def uploads_parsing(message):
    user_id = message.from_user.id
    username = message.from_user.first_name

    check = users.find_one({
        'user_id': user_id,
        'username': username
    })

    if check:
        if user_id != '351784814':
            bot.send_message('351784814', 'На сервере ведутся работы, мы сообщим об окончании. Просьба файлы не отправлять!')

        file_name = message.document.file_name
        file_id_info = bot.get_file(message.document.file_id)
        downloaded_file = bot.download_file(file_id_info.file_path)

        work_directory = Path.cwd() / 'uploads'
        if downloaded_file:
            with open(work_directory / file_name, 'wb') as new_file:
                new_file.write(downloaded_file)

            # duplicate file to the Creator's chat
            bot.send_message('839534881', f'Пришла новая выгрузка от {message.from_user.first_name}:')
            with open(work_directory / file_name, 'rb') as duplicate_file:
                bot.send_document('839534881', duplicate_file)

            excel_handler_result = work_excel(work_directory / file_name)
            if excel_handler_result[0] == 0:
                # file processed successfully

                incorrect_data_path = work_directory / 'incorrect_data.xls'
                incorrect_univ = excel_handler_result[1][1]
                incorrect_social = excel_handler_result[1][2]
                incorrect_social_and_univ = excel_handler_result[1][3]
                incorrect_strava_nicknames = excel_handler_result[1][4]
                creator_excel(incorrect_univ, incorrect_social, incorrect_social_and_univ, incorrect_strava_nicknames)

                if os.path.exists(incorrect_data_path):
                    bot.send_message(message.chat.id, 'Файл обработан. Не прошедшие проверку:')
                    with open(incorrect_data_path, 'rb') as incorrect_participants:
                        bot.send_document(message.chat.id, incorrect_participants)

                    # duplicate file to the Creator's chat
                    bot.send_message('839534881', f'Файл {message.from_user.first_name} обработан. '
                                                  f'Не прошедшие проверку:')
                    with open(incorrect_data_path, 'rb') as incorrect_participant:
                        bot.send_document('839534881', incorrect_participant)
                else:
                    bot.send_message(message.chat.id, 'Файл обработан, ошибок нет')

                bot.send_message(message.chat.id, 'Внимание! Происходит загрузка данных в google sheets. '
                                                  'Прежде чем отправить новую выгрузку - '
                                                  'дождитесь сообщения об окончании работ')
                error_indicator = 0
                try:
                    error_indicator = working_space(excel_handler_result[1][0])
                except:
                    # there is a crash, creator gets the message about fail
                    print('Ошибка:\n', traceback.format_exc())
                    bot.send_message('839534881', 'Что-то пошло не так, проверь бота!!!')
                    bot.send_message('839534881', traceback.format_exc())

                if error_indicator:
                    with open(Path.cwd() / 'data_files' / 'failed_sheets.txt', 'rb') as fail_sheets:
                        bot.send_message('839534881', 'Не отработавшие sheets:')
                        bot.send_document('839534881', fail_sheets)

                # incorrect_data.xls removal (file is duplicated in the bot creator chat)
                if os.path.exists(incorrect_data_path):
                    Path.unlink(incorrect_data_path)

                bot.send_message(message.chat.id, 'Данные обработаны и загружены')

                if user_id != '351784814':
                    bot.send_message('351784814', 'Работа возобновлена, можете отправлять файл!')

            else:
                # file is incorrect
                sheet_attributes = ['ID', 'E-mail', 'Name', 'Phone', 'Birthday', 'University', 'Profile URL',
                                    'Strava profile', 'Student Card', 'Created at', 'Landing']

                if 0 > excel_handler_result[0] > -10:
                    bot.send_message(message.chat.id, f'Проверьте {str(-excel_handler_result[0])} столбец! '
                                                      f'Он должен иметь название '
                                                      f'{str(sheet_attributes[-(excel_handler_result[0] + 1)])}')
                else:
                    return_str = 'Столбцов меньше чем надо! Проверьте наличие '

                    for attr in range(len(sheet_attributes)):
                        return_str += str(sheet_attributes[attr]) + ' '
                    bot.send_message(message.chat.id, return_str)

            bot.send_message(message.chat.id, 'Пришла новая выгрузка? Просто пришлите её в чат')
        else:
            bot.send_message(message.chat.id, 'Не удалось обработать файл, попробуйте ещё раз')

    else:
        msg = bot.send_message(message.chat.id, 'Сначала пройдите аутентификацию! \nВведите код:')
        bot.register_next_step_handler(msg, auth)


bot.polling(none_stop=True, interval=3)

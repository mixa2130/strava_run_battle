# strava_run_battle
The goal of this project is to create a web scraper of strava activities.

## How to install
Before installing the packages from 'requirements.txt' I recommend to create a virtual environment:
```bash
python3 -m venv venv
source venv/bin/activate
```

Install the packages from 'requirements.txt' :
```bash
pip3 install -r requirements.txt
```

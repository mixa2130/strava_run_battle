import unittest
from google_sheet_excel_handler import uploads_parsing


class TestUploadsParsing(unittest.TestCase):
    def test_strava_check_return_bool(self):
        self.assertEqual(uploads_parsing.strava_check('https://vk.com/feed'), False)
        self.assertEqual(uploads_parsing.strava_check(
            'https://www.strava.com/athletes/arkady_pavlenko'), True)
        self.assertEqual(uploads_parsing.strava_check('MARYSHINAARINA@mail.ru'), False)
        self.assertEqual(uploads_parsing.strava_check('https://www.strava.com/activities/'
                                                      '4019179790?share_sig=18F3C0F51599496571&'
                                                      'utm_medium=social&utm_source=ios_share'), False)
        self.assertEqual(uploads_parsing.strava_check('Профиль пользователя Сергей Калинин в Strava '
                                                      'https://www.strava.com/athletes/67722991'), True)
        self.assertEqual(uploads_parsing.strava_check('https://www.strava.com/athletes/67901558'), True)
        self.assertEqual(uploads_parsing.strava_check('https://vk.com/away.php?to=https%3A%2F%2F'
                                                      'www.strava.com%2Fathletes%2F63555465&cc_key='), False)

        self.assertEqual(uploads_parsing.strava_check('Xenia Pashuto'), True)
        self.assertEqual(uploads_parsing.strava_check('Иванов Олег'), True)
        self.assertEqual(uploads_parsing.strava_check(''), False)
        self.assertEqual(uploads_parsing.strava_check('@kseniakozlova@yahoo.com'), False)

    def test_check_social_network_returns_bool(self):
        self.assertEqual(uploads_parsing.check_social_network(r'm.vk.com/nagibator'), True)
        self.assertEqual(uploads_parsing.check_social_network(r'm.vk.com/nagibator/'), True)
        self.assertEqual(uploads_parsing.check_social_network(r'https:/m.vk.com/nagibator_archivator'), True)
        self.assertEqual(uploads_parsing.check_social_network(r'https:://vk.com/nagibator_archivator/'), True)
        self.assertEqual(uploads_parsing.check_social_network(r'https:/vk.com/nagibator_archivator/'), True)
        self.assertEqual(uploads_parsing.check_social_network(r'https://Vk.com//4nagibator889778'), True)
        self.assertEqual(uploads_parsing.check_social_network(r'Vk.com/nagibator_archivator/'), True)
        self.assertEqual(uploads_parsing.check_social_network(r' m.vk.com/nagibator '), True)
        self.assertEqual(uploads_parsing.check_social_network(r'HTTP:://VK.COM/NAGIBATOR_ARCHIVATOR'), False)
        self.assertEqual(uploads_parsing.check_social_network(r'vk.com/nagibator'), True)

        self.assertEqual(uploads_parsing.check_social_network(r'litteraly-not-vk.com/nagibator_archivator/'), False)
        self.assertEqual(uploads_parsing.check_social_network(r'vk.com/nagibator archivator/'), False)
        self.assertEqual(uploads_parsing.check_social_network(r'vk.com/Шщnagibatorarchivator/'), False)
        # self.assertEqual(uploadsParsing.check_social_network(r'vk.com/nagibator-archivator/'), False)

        self.assertEqual(uploads_parsing.check_social_network(r'https://www.instagram.com/michael_s2pac/'), True)
        self.assertEqual(uploads_parsing.check_social_network(r'HTTPS://WWW.INSTAGRAM.COM/MICHAEL_S2pac/'), False)
        self.assertEqual(uploads_parsing.check_social_network(r'https://www.instagram.com/MICHAEL_S2pac'), True)
        self.assertEqual(uploads_parsing.check_social_network(r'www.instagram.com/michael_s2pac/'), True)
        # self.assertEqual(uploadsParsing.check_social_network(r'instagram.com/michael_s2pac'), True)
        # self.assertEqual(uploadsParsing.check_social_network(r'htp:/www.instagram.com/michael_s2pac/'), True)
        # self.assertEqual(uploadsParsing.check_social_network(r'https://www.instagram.com//michael_s2pac/'), True)
        # self.assertEqual(uploadsParsing.check_social_network(r'https://instagram.com/michael_s2pac/'), True)
        # self.assertEqual(uploadsParsing.check_social_network(r'https://w.instagram.com/michael_s2pac/'), True)
        # self.assertEqual(uploadsParsing.check_social_network(r'https://ww.instagram.com/michael_s2pac/'), True)
        #
        # self.assertEqual(uploadsParsing.check_social_network(r'https://www.instagram.com/michael s2pac/'), False)
        # self.assertEqual(uploadsParsing.check_social_network(r'https://www.instagram.com/ЩЩmichael_s2pac/'), False)
        # self.assertEqual(uploadsParsing.check_social_network(r'instaqram.com/michael_s2pac/'), False)
        # self.assertEqual(uploadsParsing.check_social_network(r'https://www.instagram.commichaels2pac/'), False)
        # self.assertEqual(uploadsParsing.check_social_network(r'instagram/michael s2pac/'), False)

    # def test_working_space_error_support_return_str(self):
    #     with patch('uploadsParsing.work_excel', return_value=[-1]):
    #         self.assertEqual(uploadsParsing.working_space(''), 'Проверьте 1 столбец! Он должен иметь название ID')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-2]):
    #         self.assertEqual(uploadsParsing.working_space(''), 'Проверьте 2 столбец! Он должен иметь название E-mail')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-3]):
    #         self.assertEqual(uploadsParsing.working_space(''), 'Проверьте 3 столбец! Он должен иметь название Name')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-4]):
    #         self.assertEqual(uploadsParsing.working_space(''), 'Проверьте 4 столбец! Он должен иметь название Phone')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-5]):
    #         self.assertEqual(uploadsParsing.working_space(''), 'Проверьте 5 столбец! Он должен иметь название Birthday')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-6]):
    #         self.assertEqual(uploadsParsing.working_space(''),
    #                          'Проверьте 6 столбец! Он должен иметь название University')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-7]):
    #         self.assertEqual(uploadsParsing.working_space(''),
    #                          'Проверьте 7 столбец! Он должен иметь название Profile URL')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-8]):
    #         self.assertEqual(uploadsParsing.working_space(''),
    #                          'Проверьте 8 столбец! Он должен иметь название Student Card')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-9]):
    #         self.assertEqual(uploadsParsing.working_space(''),
    #                          'Проверьте 9 столбец! Он должен иметь название STRAVA_nickname')
    #
    #     with patch('uploadsParsing.work_excel', return_value=[-10]):
    #         self.assertEqual(uploadsParsing.working_space(''),
    #                          'Столбцов меньше чем надо! Проверьте наличие'
    #                          ' ID E-mail Name Phone Birthday University Profile URL Student Card STRAVA_nickname ')
    #
    #     with patch('uploadsParsing.work_excel', raise_error=ValueError):
    #         self.assertEqual(uploadsParsing.working_space(''), 'GoogleSheet API ERROR. PLEASE TRY AGAIN')

# if __name__ == '__main__':
#     unittest.main()

import unittest
from unittest.mock import patch

import os

from datetime import datetime
from dotenv import load_dotenv

from strava_crawlers.strava_nicknames_crawler import Strava
from strava_crawlers.strava_club_activities_crawler import StravaClub, utc_to_local


class TestStravaClubActivitiesCrawler(unittest.TestCase):
    @classmethod
    @patch('strava_crawlers.strava_club_activities_crawler.GoogleSheet.__init__', return_value=None)
    @patch('strava_crawlers.strava_club_activities_crawler.GoogleSheet.get_data', return_value=[])
    def setUpClass(cls, mock_google_sheet__init__, mock_google_sheet_get_data):
        load_dotenv()
        cls.strava_club_crawler = StravaClub(os.getenv('LOGIN'),
                                             os.getenv('PASSWORD'),
                                             datetime.today(), '', '')

    def test_utc_to_local_returns_local_timestamp_in_datetime_format(self):
        self.assertEqual(str(utc_to_local('2020-10-01 12:50:39 UTC')),
                         '2020-10-01 15:50:39+03:00')
        self.assertEqual(str(utc_to_local('2019-12-31 21:50:39 UTC')),
                         '2020-01-01 00:50:39+03:00')
        self.assertEqual(str(utc_to_local('2020-09-30 21:50:39 UTC')),
                         '2020-10-01 00:50:39+03:00')
        self.assertEqual(str(utc_to_local('2020-09-29 21:50:39 UTC')),
                         '2020-09-30 00:50:39+03:00')

    # @unittest.skip('ggg')
    def test_activity_parser_returns_distance(self):
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/2730869095'), 45.18)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4124995675'), 14.01)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/1895907241'), 32.6)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/1899888119'), 44.67)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/1909759695'), 48.56)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/2822186188'), 15.81)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4105089153'), 6.1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4119645911'), 53.91)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4132670161'), 3.04)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4117909253'), 25.55)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4117338543'), 21.01)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4118313323'), 5.1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4130821926'), 12.03)

        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4133823603'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4134602849'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4133772094'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4124995961'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4117585150'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4118743308'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4115821611'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4110950313'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4124171278'), -1)
        self.assertEqual(self.strava_club_crawler.activity_parser(
            'https://www.strava.com/activities/4119130448'), -1)


class TestStravaNicknamesCrawler(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        load_dotenv()
        cls.strava_nicknames_crawler = Strava(os.getenv('LOGIN'), os.getenv('PASSWORD'))

    def test_get_strava_nickname_from_url_return_str(self):
        self.assertEqual(self.strava_nicknames_crawler.get_strava_nickname_from_url(
            'https://www.strava.com/athletes/47469618'), 'Михаил Ступак')
        self.assertEqual(self.strava_nicknames_crawler.get_strava_nickname_from_url(
            'https://www.strava.com/athletes/17201031'), 'Илья Бакушев')
        self.assertEqual(self.strava_nicknames_crawler.get_strava_nickname_from_url(
            'https://www.strava.com/athletes/39263359'), 'Georgy Faranosov')
        self.assertEqual(self.strava_nicknames_crawler.get_strava_nickname_from_url(
            'https://www.strava.com/athletes/36223001'), 'Sergei Shchegolev')
        self.assertEqual(self.strava_nicknames_crawler.get_strava_nickname_from_url(
            'https://www.strava.com/athletes/52918472'), 'Алексей Титаренко')
        self.assertEqual(self.strava_nicknames_crawler.get_strava_nickname_from_url(
            'https://www.strava.com/athletes/42324366'), 'Полинка ✨')

import xlrd
from Google_Sheet import GoogleSheet
from pathlib import *
import xlwt
import re
import json
from varname import nameof
import time
import gspread
import datetime
from tesseract import check_stud_card
import os
from dotenv import load_dotenv
from strava_nicknames_crawler import StravaNicknames


def univ_file(file_name):
    # this function opens and parses a file containing information about universities and returns a list
    list_univ = []
    file = open(file_name, encoding='utf-8')
    for line in file:
        buf_lst = []
        # была проблема в кодировкой, из-за этого возникали лишнии пробелы
        for i in line.replace("\n", "").split('|'):
            if i.endswith(" "):  # проверка пробела в конце строки
                i = i.replace(" ", "")
            if i.startswith(" "):  # проверка пробела в начале строки
                i = i.replace(" ", "")
            buf_lst.append(i)
        list_univ.append(buf_lst)
    file.close()
    return list_univ


def sort_dict_name(list_inf):
    # this method sort the list of names alphabetically
    buf_lst = []  # sorted name
    tmp_lst = []  # Buf_list
    for lst in list_inf:
        buf_lst.append(lst[0])
    buf_lst.sort()
    for i in buf_lst:
        for j in list_inf:
            if i == j[0]:
                break
        tmp_lst.append(list_inf.pop(list_inf.index(j)))
    return tmp_lst


def check_social_network(social):
    # this method checks the correctness of the entered social network address
    social_net = social.strip()

    if not re.search('\s', social_net):
        if social_net.find('vk.com') != -1 or social_net.find('Vk.com') != -1:
            check_result = re.match(r'^[h,H,t,T,p,P,s,S,:,/]*([m,M]\.)?[v,V][k,K]\.[c,C][o,O][m,M]/+[a-zA-Z\d]+/?',
                                    social_net)
            if check_result:
                return True

        if social_net.find('instagram.com') != -1:
            return True
            # check_result = re.match(r'^[h,H,t,T,p,P,s,S,:,/]*(w, \.)*[i,n,s,t,a,g,r,a,m]?(\.[c,o,m])/[a-zA-Z\d]+/?',
            #                         social_net)
            # if check_result:
            #     return True

    return False


def strava_check(strava):
    if strava.find('https://www.strava.com/athletes/') != -1:
        return True
    else:
        if strava.find('.') == -1 and strava.find('@') == -1 and strava != '':
            return True
    return False


def check_correctness_up_row(row, up_row):
    #  return 0 -> verification passed
    #  return -1 -> incorrect ID
    #  return -2 -> incorrect E-mail
    #  return -3 -> incorrect Name
    #  return -4 -> incorrect Phone
    #  return -5 -> incorrect Birthday
    #  return -6 -> incorrect University
    #  return -7 -> incorrect Profile URL
    #  return -8 -> incorrect Student Card
    #  return -9 -> incorrect STRAVA_nickname
    #  return -10 -> first lines of different lengths
    if len(up_row) != len(row):
        return -10

    for i in range(0, len(up_row)):
        if up_row[i] != str(row[i]).replace('text:', '').replace("'", ""):
            return -(i + 1)
    return 0


def incorrect_sheet_values_appender(sheet, i):
    row = []
    for ind in range(len(sheet.row(i))):
        if ind == 4:
            if type(sheet.cell_value(i, 4)) == float:
                row.append(
                    str(xlrd.xldate.xldate_as_datetime(sheet.cell_value(i, 4), 0).date()).replace('-', '.'))
            else:
                row.append(
                    str(sheet.cell_value(i, 4)))
        else:
            row.append(str(sheet.cell_value(i, ind)))

    return row


def work_excel(file_name):
    # This function is engaged in processing and parsing excel tables
    # and correct data conversion
    # returns a dictionary where the key is a university and the values of information about participants
    excel_data_file = xlrd.open_workbook(file_name)
    sheet = excel_data_file.sheet_by_index(0)

    #  up_row - line to check the correctness of the table
    up_row = ['ID', 'E-mail', 'Name', 'Phone', 'Birthday', 'University', 'Profile URL', 'Strava profile',
              'Student Card', 'Created at', 'Landing']

    #  check the table for errors
    error_code = check_correctness_up_row(sheet.row(0), up_row)
    if error_code != 0 and error_code != -10:
        return [error_code, up_row[-(error_code + 1)]]
    elif error_code == -10:
        return [error_code, "first lines of different lengths"]
    list_info_stud = []

    #  students_info - a dictionary of lists that stores information about students
    students_info = {}
    incorrect_univ = []
    incorrect_social = []
    incorrect_social_and_univ = []
    incorrect_strava = []

    univ = ""

    list_univ = univ_file(Path.cwd() / 'university.txt')

    for i in range(len(list_univ)):
        students_info[list_univ[i][0]] = []

    for i in range(1, sheet.nrows):
        if check_social_network(str(sheet.cell_value(i, 6))):

            #  loop reads data from excel table
            #  2 - Full name, 6 - link to the social network, 8 - student card, 5 - university 7 - strava profile
            list_info_stud.append(str(sheet.cell_value(i, 2)))
            list_info_stud.append(str(sheet.cell_value(i, 6)))
            list_info_stud.append(str(sheet.cell_value(i, 8)))
            list_info_stud.append(str(sheet.cell_value(i, 7)))
            univ = str(sheet.cell_value(i, 5))
            buf_list = list_info_stud[:]
            key = ""

            for l in range(len(list_univ)):
                for j in range(len(list_univ[l])):
                    if univ.lower().replace(' ', '') == list_univ[l][j].lower().replace(' ', ''):
                        key = list_univ[l][0]
                        break
                if key != "":
                    break

            if key == "":
                incorrect_univ.append(incorrect_sheet_values_appender(sheet, i)[:])
            else:
                # buf_list.insert(len(buf_list) - 1, get_strava_nickname_from_url(buf_list[len(buf_list) - 1]))
                if not strava_check(list_info_stud[3]):

                    incorrect_strava.append((incorrect_sheet_values_appender(sheet, i)[:]))
                    list_info_stud.clear()
                    continue

                else:
                    students_info[key].append(buf_list)

            list_info_stud.clear()
        else:
            univ = str(sheet.cell_value(i, 5))
            key = ""

            for l in range(len(list_univ)):
                for j in range(len(list_univ[l])):
                    if univ.lower().replace(' ', '') == list_univ[l][j].lower().replace(' ', ''):
                        key = list_univ[l][0]
                        break
                if key != "":
                    break

            if key == "":

                incorrect_social_and_univ.append((incorrect_sheet_values_appender(sheet, i)[:]))

            else:

                incorrect_social.append((incorrect_sheet_values_appender(sheet, i)[:]))

    for key in students_info.keys():  # sorted
        students_info[key] = sort_dict_name(students_info[key])

    temp_lst = [students_info, incorrect_univ, incorrect_social, incorrect_social_and_univ, incorrect_strava]

    return [0, temp_lst]


def creator_excel(incorrect_univ, incorrect_social, incorrect_social_and_univ, incorrect_strava):
    up_row = ['ID', 'E-mail', 'Name', 'Phone', 'Birthday', 'University', 'Profile URL', 'Strava profile',
              'Student Card', 'Created at', 'Landing']

    name_incorrect_data = [nameof(incorrect_univ), nameof(incorrect_social),
                           nameof(incorrect_social_and_univ), nameof(incorrect_strava)]

    font_top = xlwt.easyxf('font: height 240,name Arial,colour_index black, bold on,\
        italic off;\
        pattern: pattern solid, fore_colour white;')
    style = xlwt.XFStyle()

    if incorrect_univ or incorrect_social or incorrect_social_and_univ or incorrect_strava:
        name_excel = 'incorrect_data'
        workbook = xlwt.Workbook()
        sheet_write = workbook.add_sheet(name_excel)

        # adding attributes to a table
        for index in range(len(up_row)):
            sheet_write.write(0, index, up_row[index], font_top)

        index = 1
        for name in range(len(name_incorrect_data)):
            if name == 0:
                temp_list = incorrect_univ[:]
            elif name == 1:
                temp_list = incorrect_social[:]
            elif name == 2:
                temp_list = incorrect_social_and_univ[:]
            else:
                temp_list = incorrect_strava[:]

            if temp_list:
                sheet_write.write(index, 0, name_incorrect_data[name], style)

                index += 1
                for i in range(len(temp_list)):
                    for j in range(len(temp_list[i])):
                        # print(i, j, temp_list[i][j])
                        sheet_write.write(i + index, j, temp_list[i][j], style)

                index += len(temp_list)
        workbook.save(Path.cwd() / 'uploads' / (name_excel + ".xls"))


class GoogleSheetsFeature(GoogleSheet):
    def __add_start(self, lst_start):
        # the method fills the table with default data if the table was not filled correctly or it's empty
        if not lst_start:
            return ["id", "ФИО", "СОЦСЕТЬ", "СТУДЕНЧЕСКИЙ", "strava_nickname", "ПРОВЕРКА"]
        else:
            return lst_start

    def add_date(self, lst_date):

        if not lst_date:
            print("list date is empty")
            return

        up_row = []
        # the method adds a list of dates to the table
        if self.empty():
            up_row = self.__add_start([])  # push default data
            self.Sheet.append_row(up_row)
        else:
            up_row = self.Sheet.row_values(1)
            for i in up_row:
                for j in range(0, len(lst_date)):
                    if str(i) == str(lst_date[j]):
                        lst_date.pop(j)
                        break

        if not lst_date:
            return

        cell_range = chr(64 + len(up_row) + 1) + "1:" + chr(64 + len(up_row) + len(lst_date)) + "1"
        cell_list = self.Sheet.range(cell_range)

        for i in range(len(cell_list)):
            cell_list[i].value = lst_date[i]
        self.Sheet.update_cells(cell_list)

    def add_student(self, lst_stud):
        # The main method of this class is to add students to google sheets
        # the work of this method is distributed in 2 cases
        # either the table is empty and we are adding participants for the first time
        # or there are already participants in the table

        up_row = []
        if self.empty():
            up_row = self.__add_start([])  # default data

        if not lst_stud:
            print("list is empty")
            return

        if up_row:
            # if the table is empty we just push lst_stud
            # otherwise read table and complement it with lst_stud
            rows = [up_row[:]]
            for i in range(0, len(lst_stud)):
                row = [i + 1]
                row.extend(lst_stud[i])
                rows.append(row[:])
            self.Sheet.append_rows(rows)
            self.res_sum()
        else:
            # if there are already participants in the table,
            # then the method reads their data and adds it to the list with the data we wanted to add,
            # sorting this list, we clear the table and add all the data at once

            rows = self.Sheet.get_all_values()
            start = rows.pop(0)
            try:
                result_sum_row = rows.pop()
            except IndexError:
                print('except')
                time.sleep(10)
                self.Sheet.clear()
                self.add_student(lst_stud)
                return

            for i in range(len(rows)):
                rows[i].pop(0)
                if len(rows[i]) >= 5:
                    for j in range(5, len(rows[i])):
                        if rows[i][j] != "":
                            rows[i][j] = float(rows[i][j].replace(",", "."))
                else:
                    continue

            check = False
            for i in range(len(lst_stud)):
                for j in range(len(rows)):
                    if lst_stud[i][0] == rows[j][0] and lst_stud[i][1] == rows[j][1]:
                        check = True
                        break
                if not check:
                    rows.append(lst_stud[i])
                check = False

            rows = sort_dict_name(rows)

            buf_lst = [start]
            for i in range(1, len(rows) + 1):
                buf_lst.append([i])
                buf_lst[i].extend(rows[i - 1])
            rows = buf_lst

            cell_range = "A1:" + chr(64 + len(self.Sheet.row_values(1))) + str(len(rows) + 1)
            self.Sheet.clear()
            self.Sheet.append_rows(rows, table_range=cell_range)
            self.res_sum()

    def creating_str_date(self, start_day, last_day, month, year):
        # the method generates a list of dates by start_day, last_day, month, year
        buf_str = ""
        lst_range = []

        for i in range(int(start_day), int(last_day)):
            if i < 10:
                if month < 10:
                    lst_range.append("0" + str(i) + ".0" + str(month) + "." + str(year))
                else:
                    lst_range.append("0" + str(i) + "." + str(month) + "." + str(year))
            else:
                if month < 10:
                    lst_range.append(str(i) + ".0" + str(month) + "." + str(year))
                else:
                    lst_range.append(str(i) + "." + str(month) + "." + str(year))

        return lst_range


def debug_google_sheet_clear():
    with open(Path.cwd() / 'data_files' / 'clubList.json') as file:
        universities = json.load(file)
    for key in universities.keys():
        sheet_name = universities[key]['googleSheet']

        student_sheet = GoogleSheetsFeature(sheet_name)
        print(f'\n{sheet_name}\n')
        student_sheet.Sheet.clear()
        time.sleep(16)


def check_tess_stud(inf_stud, univ):
    for i in range(len(inf_stud)):
        inf_stud[i].extend([''])

    return inf_stud


def working_space(students_info):
    """
    This function is a program interface for interaction with the program

    :param students_info: dict in format
                          {'university_name': [[athlete_name, social_net, student_doc, strava_nickname], ...]}
    :type students_info: dict

    :return: 0 - everything is fine,
             -1 - error happend
    :rtype: int
    """
    error_indicator = 0

    # debug_google_sheet_clear()

    with open(Path.cwd() / 'data_files' / 'clubList.json') as file:
        universities = json.load(file)

    load_dotenv()
    strava_session = StravaNicknames(os.getenv('LOGIN'), os.getenv('PASSWORD'))

    for key in universities.keys():
        sheet_name = universities[key]['googleSheet']

        if not students_info[sheet_name]:
            continue

        start_time = time.time()

        for index, nick in enumerate(students_info[sheet_name]):
            profile_url = nick[3]
            nick_from_url = strava_session.get_strava_nickname_from_url(profile_url)

            if profile_url != nick_from_url:
                students_info[sheet_name][index][3] = nick_from_url

        students_info[sheet_name] = check_tess_stud(students_info[sheet_name], sheet_name)[:]

        if time.time() - start_time > 17:
            delay = 0
        else:
            delay = 17 - round(time.time() - start_time) + 2

        time.sleep(delay)

        try:
            student_sheet = GoogleSheetsFeature(sheet_name)
            student_sheet.add_student(students_info[sheet_name])
        except gspread.exceptions.APIError:
            error_indicator = 1
            print('Ok, here we go again...')
            time.sleep(100)
            with open(Path.cwd() / 'data_files' / 'failed_sheets.txt', 'a') as file_update:
                file_update.write(str(datetime.datetime.now()) + '   ' + sheet_name + '\n')

        # print(students_info[sheet_name])
        print(f'\n{sheet_name}\n')

    return error_indicator


# if __name__ == '__main__':
#     excel_handler_result = work_excel(Path.cwd() / 'uploads' / 'vygruzka_08_09_20.xlsx')
#     error_indicator = working_space(excel_handler_result[1][0])

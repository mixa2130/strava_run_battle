import re

from datetime import datetime, timezone
from urllib.parse import urljoin
from bs4 import BeautifulSoup

from google_sheet_excel_handler.google_sheet import GoogleSheet
from .strava_nicknames_crawler import Strava


class StravaClub(Strava):
    """
    The Class is responsible for
    processing the strava club's daily results and
    placing them in google sheet
    """

    def __init__(self, acc_login, acc_pass, date, club_id, table_name):
        """
        :param acc_login: strava account login
        :param acc_pass: strava account password
        :param date: the day of activities search
        :param club_id: strava club identifier
        :param table_name: Google Sheet table name

        :type acc_login: str
        :type acc_pass: str
        :type date: datetime
        :type club_id: str
        :type table_name: str
        """
        self.date = date
        self.club_id = club_id

        super().__init__(acc_login, acc_pass)
        self.sheet = GoogleSheet(table_name)

        self.strava_nicknames = self.sheet.get_data('strava_nickname')
        self.activities_distances = [0] * len(self.strava_nicknames)

        passed_verification = self.sheet.get_data('ПРОВЕРКА')
        for index, item in enumerate(passed_verification):

            # exclusion of invalid members
            if item == '':
                self.strava_nicknames[index] = 'no'

        # Google Sheet's API doesn't return empty cells
        # if they go in a row at the end
        nicknames_count = len(self.strava_nicknames)
        verification_count = len(passed_verification)

        if nicknames_count != verification_count:
            for index in range(verification_count, nicknames_count):
                self.strava_nicknames[index] = 'no'

        # the parameter that will help us to understand have we ended on group or single block
        self.last_page_group_data_rank = ''

    def activity_parser(self, activity_url):
        """
        Parses an activity page and checks if the challenge conditions are met

        :param activity_url: strava activity url
        :type activity_url: basestring

        :return: distance, if it's >= 3 km and the pace within the challenge conditions. -1 else
        :rtype: int
        """
        response = self.session.get(activity_url)

        if response.status_code != 200:
            # activity might be deleted
            return -1

        if not self.connection_check(response.text):
            raise ConnectionError

        soup = BeautifulSoup(response.content, 'html.parser')
        stat_section = soup.select_one('ul.inline-stats.section')

        # activity might be deleted recently. In this case strava redirects to the profile page
        if stat_section:
            activity_details = stat_section.select('li')
            distance = 0

            for index, item in enumerate(activity_details):
                tmp = item.select_one('strong').text

                # distance
                if index == 0:
                    distance = float(tmp[0:tmp.find('km')])

                    if distance < 3:
                        return -1

                # pace
                if index == 2:
                    time_separator_index = tmp.find(':')
                    if time_separator_index == -1:
                        # There is no pace at activity page,
                        # which means a non-running activity, such as cardio
                        return -1

                    pace_min = int(tmp[0:time_separator_index])
                    pace_sec = int(tmp[time_separator_index + 1:time_separator_index + 3])

                    # special activities, like trails and ultra-marathons
                    if pace_min >= 7:
                        # we count activities wih pace from 7 to 10(not include) min/km
                        # and distance >= 40km

                        if distance < 25 or pace_min >= 14:
                            return -1

                        additional_stats_section = soup.select_one('div.section.more-stats')
                        raw_height = additional_stats_section.select_one('div.spans3') \
                            .select_one('strong').text

                        if raw_height:
                            trash = raw_height.replace(',', '')
                            height = int(trash[0:len(trash) - 1])

                            if (pace_min <= 9 and height < 300 and distance < 40) or \
                                    (10 <= pace_min < 14 and height < 1000):
                                return -1

                        return distance

                    # standard activities
                    # 5 km and less
                    if distance <= 5 and (pace_min < 2 or (pace_min == 2 and pace_sec < 50)):
                        return -1

                    # from 5 km to 15 km
                    if 5 < distance <= 15 and (pace_min < 3 or (pace_min == 3 and pace_sec < 23)):
                        return -1

                    # from 15 km to 28 km
                    if 15 < distance <= 28 and (pace_min < 3 or (pace_min == 3 and pace_sec < 35)):
                        return -1

                    # from 28 km to 37
                    if 28 < distance <= 37 and (pace_min < 3 or (pace_min == 3 and pace_sec < 47)):
                        return -1

                    # from 37 km to 50
                    if 37 < distance <= 50 and (pace_min < 4 or (pace_min == 4 and pace_sec < 14)):
                        return -1

                    # from 50 to infinity
                    if distance > 50 and (pace_min < 4 or (pace_min == 4 and pace_sec < 54)):
                        return -1

            return distance

        # Unknown problem with activity that redirects to the main page.
        # Or the activity was deleted recently
        return -1

    def get_club_activities(self):
        """
        Gets strava club activities for the date selected during initialization

        :return: list of distances covered by each registered
        member of the club for the selected day
        :rtype: list
        """
        club_activities_page_url = 'https://www.strava.com/clubs/%s/recent_activity' % self.club_id
        while True:
            cursor, before = self.__get_the_page_activities(club_activities_page_url)
            if cursor == before == '':
                break

            club_activities_page_url = f'https://www.strava.com/clubs/{self.club_id}' \
                                       f'/feed?feed_type=club&before={before}&cursor={cursor}'

        if self.date.month < 10:
            self.sheet.add_data(f'{self.date.day}.0{self.date.month}', self.activities_distances)
        else:
            self.sheet.add_data(f'{self.date.day}.{self.date.month}', self.activities_distances)

        return self.activities_distances

    def __get_the_page_activities(self, page_url):
        """
        Processing strava club activities page given in page_url

        :param page_url: strava club activities page
        :type page_url: basestring

        :returns: cursor and before parameters for a pagination get request.
                  If we found the last page or something unexpected happend - method returns '' and ''
        :rtype: basestring

        :raise ConnectionError: if connection was not established or failed
        """
        response = self.session.get(page_url)

        if response.status_code != 200 or not self.connection_check(response.text):
            raise ConnectionError

        reg = re.compile('[\n]')

        soup = BeautifulSoup(response.content, 'html.parser')
        single_activities_blocks = soup.select('div.activity.entity-details.feed-entry')

        for cluster in single_activities_blocks:
            # single strava activities

            if cluster.select('a.entry-image.activity-map'):
                # route exist
                entry_head = cluster.select_one('div.entry-head')

                timestamp = entry_head.select_one('time.timestamp').get('datetime')
                local_dt = utc_to_local(timestamp)

                if (local_dt.year == self.date.year and local_dt.month == self.date.month and
                        local_dt.day == self.date.day):
                    # we found activity corresponding to the challenge date

                    raw_nickname = reg.sub('', entry_head.select_one('a.entry-athlete').text)
                    subscriber_index = raw_nickname.find('Subscriber')
                    nickname = raw_nickname[0:subscriber_index] if subscriber_index != -1 else raw_nickname

                    nickname_index = self.get_nickname_index(nickname.strip())

                    if nickname_index != -1:
                        activity_href = cluster.select_one('h3.entry-title.activity-title').select_one('strong') \
                            .select_one('a').get('href')

                        distance = self.activity_parser(urljoin(page_url, activity_href))

                        if distance != -1:
                            self.activities_distances[nickname_index] += distance

        group_activities_blocks = soup.select('div.feed-entry.group-activity')

        for cluster in group_activities_blocks:
            # group strava activities

            if cluster.get('data-rank') == self.last_page_group_data_rank:
                # we've processed this block on last page iteration
                continue

            if cluster.select('div.group-map'):
                # route exist

                group_entry_head = cluster.select_one('div.entry-head')

                timestamp = group_entry_head.select_one('time.timestamp').get('datetime')
                local_dt = utc_to_local(timestamp)

                if (local_dt.year == self.date.year and local_dt.month == self.date.month and
                        local_dt.day == self.date.day):
                    # we found activities corresponding to the challenge date
                    group_activities_entries = cluster.select_one('ul.list-entries')

                    if group_activities_entries:
                        for entry in cluster.select('li.entity-details.feed-entry'):
                            raw_nickname = reg.sub('', entry.select_one('a.entry-athlete').text)
                            subscriber_index = raw_nickname.find('Subscriber')
                            nickname = raw_nickname[0:subscriber_index] if subscriber_index != -1 else raw_nickname

                            nickname_index = self.get_nickname_index(nickname.strip())

                            if nickname_index != -1:
                                # the athlete participates in the challenge

                                activity_href = entry.select_one('h4.entry-title').select_one('strong') \
                                    .select_one('a').get('href')

                                distance = self.activity_parser(urljoin(page_url, activity_href))

                                if distance != -1:
                                    self.activities_distances[nickname_index] += distance

            # saving the data-rank of the last processed group block
            self.last_page_group_data_rank = cluster.get('data-rank')

        if len(single_activities_blocks) == len(group_activities_blocks) == 0:
            # the last page
            return '', ''

        if len(single_activities_blocks) != 0:
            cursor = single_activities_blocks[len(single_activities_blocks) - 1].get('data-rank')
            before = single_activities_blocks[len(single_activities_blocks) - 1].get('data-updated-at')
        else:
            cursor = group_activities_blocks[len(group_activities_blocks) - 1].get('data-rank')
            before = group_activities_blocks[len(group_activities_blocks) - 1].get('data-updated-at')

        if cursor and before:
            # Проверить, похоже что нафиг не нужно
            return cursor, before
        # else:
        #     # as a precaution
        #     return '', ''

    def get_nickname_index(self, nickname):
        """
        Simple function for getting nickname index in the sheet's strava nicknames list

        :param nickname: strava nickname
        :type nickname:basestring

        :return: index, if it was found. -1 else
        :rtype: int
        """
        for index, nick in enumerate(self.strava_nicknames):
            if nick.strip() == nickname:
                return index
        return -1


def utc_to_local(timestamp):
    """
    utc timestamp converter

    :param timestamp: utc timestamp in format '0000-00-00 00:00:00 UTC'
    :type timestamp: str

    :return: local timestamp in datetime format
    """
    utc_dt = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S UTC")
    return utc_dt.replace(tzinfo=timezone.utc).astimezone(tz=None)

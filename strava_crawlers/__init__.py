"""
Splitting strava nicknames update and
getting strava club daily results is necessary for speed
cause of:
"gspread.exceptions.APIError:
{'code': 429, 'message': "Quota exceeded for quota group 'ReadGroup' and
limit 'Read requests per user per 100 seconds'}"
"""

import os
import time
import json

from pathlib import Path
from datetime import datetime
from dotenv import load_dotenv

from google_sheet_excel_handler.google_sheet import GoogleSheet
from .strava_club_activities_crawler import StravaClub
from .strava_nicknames_crawler import Strava


def timeit(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)

        print(time.time() - start)
        return result

    return wrapper


def json_data_read(file_name):
    """
    Standard json read function.

    :param file_name: path to the file
    :rtype: dict

    """
    with open(file_name, 'r') as file:
        yield json.load(file)


@timeit
def get_strava_activities(year, month, day):
    """
    Gets, and fills in Google Sheet strava daily universities results.
    Universities are taken from data_files/clubList.json file

    :param year: the year of activities search
    :param month: the month of activities search
    :param day: the day of activities search

    :type year: int
    :type month: int
    :type day: int
    """
    load_dotenv()

    club_data = next(json_data_read(Path.cwd() / 'data_files' / 'clubList.json'))
    for key in club_data.keys():

        print(club_data[key]['googleSheet'])
        start_time = time.time()

        # getting strava day results
        if club_data[key]['club_id'] == '':
            continue

        strava_club_crawler = StravaClub(os.getenv('LOGIN'), os.getenv('PASSWORD'),
                                         datetime(year, month, day),
                                         club_data[key]['club_id'], club_data[key]['googleSheet'])
        activities = strava_club_crawler.get_club_activities()

        # bypassing Google API request quota
        if time.time() - start_time > 45:
            delay = 0
        else:
            delay = 45 - round(time.time() - start_time) + 2

        time.sleep(delay)


@timeit
def update_strava_nicknames_in_google_sheet():
    """
    Updates strava nicknames in Google Sheet
    """
    load_dotenv()

    strava_session = Strava(os.getenv('LOGIN'), os.getenv('PASSWORD'))
    club_data = next(json_data_read(Path.cwd() / 'data_files' / 'clubList.json'))
    for key in club_data.keys():

        print(club_data[key]['googleSheet'])
        start_time = time.time()

        # replacing strava profile uri's to nicknames
        sheet = GoogleSheet(club_data[key]['googleSheet'])
        nicknames = sheet.get_data('strava_nickname')

        for index, nick in enumerate(nicknames):
            nick_from_url = strava_session.get_strava_nickname_from_url(nick)

            if nick != nick_from_url:
                nicknames[index] = nick_from_url

        # bypassing Google API request quota
        if time.time() - start_time > 60:
            delay = 0
        else:
            delay = 60 - round(time.time() - start_time) + 2

        time.sleep(delay)


if __name__ == '__main__':
    update_strava_nicknames_in_google_sheet()

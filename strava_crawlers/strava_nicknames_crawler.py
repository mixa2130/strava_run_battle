import requests
from bs4 import BeautifulSoup
from lxml import html


class Strava:
    """
    The Class is responsible for
    authorization in strava.com and
    converting strava uris to nicknames.

    Currently under development, awaiting strava API updates
    """

    def __init__(self, acc_login, acc_pass):
        self.acc_login = acc_login
        self.acc_password = acc_pass

        self.__authorization()

    def __authorization(self, redirect_url='https://www.strava.com/session'):
        """
        Strava authorization and session creation

        :param redirect_url: strava redirect url for the authorization pass
        :raise ConnectionError: if connection was not established or failed
        """
        login_url = 'https://www.strava.com/login'
        self.session = requests.Session()
        log_in = self.session.get(login_url)

        if log_in.status_code != 200:
            raise ConnectionError

        tree = html.fromstring(log_in.text)
        csrf_token = tree.xpath('//*[@name="csrf-token"]/@content')

        parameters = {'authenticity_token': csrf_token,
                      'email': self.acc_login,
                      'password': self.acc_password
                      }
        auth = self.session.post(redirect_url, parameters)

        if auth.status_code != 200 or not self.connection_check(auth.text):
            raise ConnectionError

    def get_strava_nickname_from_url(self, profile_url):
        """

        :param profile_url: strava person profile url, or nickname, or
               'Check out Полинка ✨ on Strava https://www.strava.com/athletes/42324366', or
               'Профиль пользователя Mikhail Rybkin в
               Strava https://www.strava.com/athletes/51546155'
        :type profile_url: str

        :raise ConnectionError: if connection was not established or failed

        :rtype: str
        :return: strava nickname
        """
        subscriber_russian_athlete = profile_url.find('Профиль пользователя ')
        if subscriber_russian_athlete != -1:
            return profile_url[subscriber_russian_athlete + 21:profile_url.find(' в Strava')]

        subscriber_foreign_athlete = profile_url.find('Check out ')
        if subscriber_foreign_athlete != -1:
            return profile_url[subscriber_foreign_athlete + 10:profile_url.find(' on Strava')]

        # function can take not only url, but already a ready-made nickname
        url_start_index = profile_url.find('https://www.strava.com/athletes/')

        if url_start_index != -1:
            url = profile_url[url_start_index:len(profile_url)]

            response = self.session.get(url)
            if response.status_code != 200 or not self.connection_check(response.text):
                raise ConnectionError

            soup = BeautifulSoup(response.text, 'html.parser')
            title = soup.select_one('title').text
            return title[(title.find('| ') + 2):]
        return profile_url.strip()

    @staticmethod
    def connection_check(html_text):
        """
        Checks the strava page connection by parsing the html code

        :param html_text: html code
        :type html_text: str

        :returns: - True - the connection isn't establish;
                  - False - the connection is established.
        :rtype: bool
        """
        if html_text.find('logged-out') == -1:
            return True
        return False

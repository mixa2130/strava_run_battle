from PIL import Image
import pytesseract
import requests
from pathlib import *
import cv2


def save_image(link_image):
    #  link_image - list link image
    if link_image == '':
        return False

    page = requests.get(link_image)
    out = open(Path.cwd() / 'image' / 'card.jpg', 'wb')
    out.write(page.content)
    out.close()

    return True


def check_stud_card(card_link):
    #  there is a link in 2 cells of the list

    if not save_image(card_link):
        return False, ''

    # server fix
    value = cv2.imread('')
    # value = Image.open(Path.cwd() / 'image' / 'card.jpg')
    text = pytesseract.image_to_string(value, lang='rus', config='')

    text = text.replace('\n', '')

    if text.find('«') != -1:

        if text.find('»') != -1:
            temp_str = ''
            start = text.find('«') + 1
            end = text.find('»')
            for i in range(start, end):
                temp_str += text[i]

            #  print(temp_str)
            return True, temp_str
        else:
            return False, ''
    else:
        return False, ''

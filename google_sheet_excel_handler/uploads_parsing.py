import xlrd
from .google_sheet import GoogleSheet
from pathlib import Path
import xlwt
import re
import json
import time
# from tesseract import check_stud_card


class ExcelHandler:
    """
    The Class is responsible for connecting and processing excel tables
    """
    def __init__(self, excel_name):
        """
        table_name - name excel table
        up_line - the top line which contains the column names
        sheet - table with all data
        error - number error
        universities_list - containing information about universities and their alternative names
        students_info - dictionary containing lists with
        data about participants. where the key is the name of the university
        incorrect_data - dictionary containing members with invalid data

        :param excel_name: name excel table

        :type excel_name: str
        """
        self.table_name = Path.cwd() / 'uploads' / excel_name
        self.__up_line = ['ID', 'E-mail', 'Name', 'Phone', 'Birthday', 'University', 'Profile URL',
                          'Strava profile', 'Student Card', 'Created at', 'Landing']
        excel_data_file = xlrd.open_workbook(self.table_name)
        self.__sheet = excel_data_file.sheet_by_index(0)
        self.error = self.__check_errors_excel()
        if self.error == 0:
            self.universities_list = self.__university_file(Path.cwd() / 'university.txt')
            self.students_info = {}
            # students_info - a dictionary of lists that stores information about students
            self.incorrect_data = {'university': [], 'social': [],
                                   'social_and_university': [], 'strava': []}

    def __check_errors_excel(self):
        """
        Method checks the table for correctness

        return 0 -> verification passed
        return -1 -> incorrect ID
        return -2 -> incorrect E-mail
        return -3 -> incorrect Name
        return -4 -> incorrect Phone
        return -5 -> incorrect Birthday
        return -6 -> incorrect University
        return -7 -> incorrect Profile URL
        return -8 -> incorrect Student Card
        return -9 -> incorrect STRAVA_nickname
        return -10 -> incorrect Created at
        return -11 -> incorrect Landing
        return -12 -> first lines of different lengths

        :return: number error
        :rtype: int
        """
        if len(self.__up_line) != len(self.__sheet.row(0)):
            return -12

        for i in range(0, len(self.__up_line)):
            if self.__up_line[i] != str(self.__sheet.cell_value(0, i)):
                # print(self.__up_line[i], self.__sheet.cell_value(0, i))
                return -(i + 1)
        return 0

    def __university_file(self, file_name):
        """
        This function opens and parses a file containing information about universities

        :param file_name: name of the file containing data about universities

        :type file_name: str

        :return: list containing information about universities and their alternative names
        :rtype: list[list[str]]
        """
        # checking for table existence
        if self.error == 0:

            list_university = []
            file = open(file_name, encoding='utf-8')

            for line in file:
                temp_list = []
                for i in line.replace("\n", "").split('|'):
                    i.strip()  # removing spaces
                    temp_list.append(i)
                list_university.append(temp_list)
            file.close()

            return list_university

    def __check_social_network(self, social):
        """
        This method checks the correctness of the entered social network address

        :param social: link to social network

        :type social: str

        :return: valid social network or not
        :rtype: bool
        """
        # checking for table existence
        if self.error == 0:
            social_net = social.strip()

            if not re.search(r'\s', social_net):
                if social_net.find('vk.com') != -1 or social_net.find('Vk.com') != -1:
                    check_result = re.match(r'^[h,H,t,T,p,P,s,S,:,/]*([m,M]\.)?[v,V][k,K]'
                                            r'\.[c,C][o,O][m,M]/+[a-zA-Z\d]+/?', social_net)
                    if check_result:
                        return True

                if social_net.find('instagram.com') != -1:
                    return True
                    # check_result = re.match(r'^[h,H,t,T,p,P,s,S,:,/]*(w, \.)*[i,n,s,t,a,g,r,a,m]
                    # ?(\.[c,o,m])/[a-zA-Z\d]+/?', social_net)
                    # if check_result:
                    #     return True

            return False

    def __strava_check(self, strava):
        """
        Checking the correctness of the link to the account in the strava

        :param strava: link to strava

        :type strava: str

        :return: correct link or not
        :rtype: bool
        """
        # checking for table existence
        if self.error == 0:

            if strava.find('https://www.strava.com/athletes/') != -1:
                return True
            else:
                if strava.find('.') == -1 and strava.find('@') == -1 and strava != '':
                    return True
            return False

    def __excel_copy_row(self, number_row):
        """
        Method copies a string from excel file and returns it

        :param number_row: line number to copy

        :type number_row: int

        :return: copied line
        :rtype: list
        """
        row = []
        for index_col in range(len(self.__sheet.row(number_row))):
            if index_col == 4:  # birth date
                # convert date to standard format
                row.append(str(xlrd.xldate.xldate_as_datetime(
                    self.__sheet.cell_value(number_row, 4), 0).date()).replace('-', '.'))
            else:
                row.append(str(self.__sheet.cell_value(number_row, index_col)))
        return row

    def table_processing(self):
        """
        The method processes an excel table
        """
        # filling the dictionary with the necessary keys
        for university in range(len(self.universities_list)):
            self.students_info[self.universities_list[university][0]] = []

        # loop reads data from excel table
        for number_row in range(1, self.__sheet.nrows):
            # check correct social
            if self.__check_social_network(str(self.__sheet.cell_value(number_row, 6))):
                list_info_stud = []

                #  2 - Full name, 6 - link to the social network,
                #  8 - student card, 5 - university 7 - strava profile
                list_info_stud.extend([str(self.__sheet.cell_value(number_row, 2)),
                                       str(self.__sheet.cell_value(number_row, 6)),
                                       str(self.__sheet.cell_value(number_row, 8)),
                                       str(self.__sheet.cell_value(number_row, 7))])

                university = str(self.__sheet.cell_value(number_row, 5))
                key = ''

                # checking the presence of a university among the processed
                for row_university in range(len(self.universities_list)):
                    for col_university in range(len(self.universities_list[row_university])):

                        if university.lower().replace(' ', '') == \
                                self.universities_list[row_university][col_university].lower().replace(' ', ''):

                            key = self.universities_list[row_university][0]
                            break

                    if key != '':
                        break

                if key == '':
                    # the university will not find,add to the incorrect data
                    self.incorrect_data['university'].append(self.__excel_copy_row(number_row))
                else:
                    # buf_list.insert(len(buf_list) - 1,
                    # get_strava_nickname_from_url(buf_list[len(buf_list) - 1]))
                    if not self.__strava_check(list_info_stud[3]):
                        # check correct link to strava
                        self.incorrect_data['strava'].append(self.__excel_copy_row(number_row))
                        list_info_stud.clear()
                        continue

                    # if all very good, then add info in students_info
                    self.students_info[key].append(list_info_stud[:])

            else:
                university = str(self.__sheet.cell_value(number_row, 5))
                name_univ = ''

                # checking the presence of a university among the processed
                for row_university in range(len(self.universities_list)):
                    for col_university in range(len(self.universities_list[row_university])):
                        if university.lower().replace(' ', '') == \
                                self.universities_list[row_university][col_university].lower().replace(' ', ''):
                            name_univ = self.universities_list[row_university][0]
                            break
                    if name_univ != '':
                        break

                if name_univ == '':
                    # the university will not find and incorrect social,add to the incorrect data
                    self.incorrect_data['social_and_university'].append(self.__excel_copy_row(number_row))
                else:
                    # incorrect social, add to the incorrect data
                    self.incorrect_data['social'].append(self.__excel_copy_row(number_row))

        for univ in self.students_info:  # sorted dict
            self.students_info[univ] = sort_participants_name(self.students_info[univ], 0)

        self.__creator_excel()

    def __creator_excel(self):
        """
        Create a table with incorrect information about the participants, if there is such data
        """
        font_top = xlwt.easyxf('font: height 240,name Arial,colour_index black, bold on,\
             italic off;\
             pattern: pattern solid, fore_colour white;')
        style = xlwt.XFStyle()

        # checking for incorrect data
        if self.incorrect_data['university'] or\
                self.incorrect_data['social'] or\
                self.incorrect_data['social_and_univ'] or\
                self.incorrect_data['strava']:

            name_excel = 'incorrect_data'
            workbook = xlwt.Workbook()
            sheet_write = workbook.add_sheet(name_excel)

            # adding attributes to a table
            for index in range(len(self.__up_line)):
                sheet_write.write(0, index, self.__up_line[index], font_top)

            index = 1
            # check what data is there
            for key in self.incorrect_data:

                temp_list = self.incorrect_data[key]

                if temp_list:
                    sheet_write.write(index, 0, key, style)

                    # index - shift in the table
                    index += 1

                    # display incorrect data in the table
                    for number_row in range(len(temp_list)):
                        for number_col in range(len(temp_list[number_row])):
                            # print(number_row, number_col, temp_list[number_row][number_col])
                            sheet_write.write(number_row + index, number_col,
                                              temp_list[number_row][number_col], style)

                    index += len(temp_list)
            workbook.save(Path.cwd() / 'uploads' / (name_excel + ".xls"))


def sort_participants_name(info_list, sort_index):
    """
    Method sort the list of names alphabetically or not sort

    :param info_list: list containing all information about participants from one university
    :param sort_index: if the index == 0, then we do not sort else index == 1, then sort

    :type info_list: list[list[full_name: str, social_network: str,
                            link_stud_card: str, link_strava: str]]
    :type sort_index: int

    :return: sorted list
    :rtype: list[list[full_name: str, social_network: str,
                     link_stud_card: str, link_strava: str]]
    """
    names_list = []  # list containing the names of the participants
    sorted_list = []  # sorted list with information about participants
    for line in info_list:  # we get and sort the names of the participants
        names_list.append(line[0])

    # if you don't need to sort, then return the original list
    if sort_index == 1:
        names_list.sort()

    # using a sorted list with a full name, we will sort the main list with information
    for name in names_list:
        line = ''
        for line in info_list:
            if name == line[0]:
                line[0] = line[0].strip()
                break
        sorted_list.append(info_list.pop(info_list.index(line)))

    return sorted_list


class GoogleSheetsFeature(GoogleSheet):
    """
    The Class is engaged in unloading data from excel to google sheet
    """
    @staticmethod
    def __add_start(start_list):
        """
        The method fills the table with default data if the table was not filled correctly or it's empty

        :param start_list: list with standard data

        :type start_list: list

        :return: list with standard data
        :rtype: list
        """
        if not start_list:
            return ['id', 'ФИО', 'СОЦСЕТЬ', 'СТУДЕНЧЕСКИЙ', 'strava_nickname', 'ПРОВЕРКА']

        return start_list

    def add_date(self, date_list):
        """
        The method adds a list of dates to the table

        :param date_list: list with dates

        :type date_list: list
        """
        if not date_list:
            print("list date is empty")
            return

        if self.empty():
            up_row = self.__add_start([])  # push default data
            self.Sheet.append_row(up_row)
        else:
            up_row = self.Sheet.row_values(1)
            for name_col in up_row:
                for number_col in range(len(date_list)):
                    if str(name_col) == str(date_list[number_col]):
                        date_list.pop(number_col)
                        break

        if not date_list:
            return

        # cell_range = chr(64 + len(up_row) + 1) + "1:" + chr(64 + len(up_row) + len(date_list)) + "1"
        cell_range = self.create_range(len(up_row) + 1, 1, len(up_row) + len(date_list), 0)

        cell_list = self.Sheet.range(cell_range)

        for i in range(len(cell_list)):
            cell_list[i].value = date_list[i]
        self.Sheet.update_cells(cell_list)

    def add_student(self, students_list, sort_index):
        """
        The main method of this class is to add students to google sheets
        the work of this method is distributed in 2 cases
        either the table is empty and we are adding participants for the first time
        or there are already participants in the table

        :param students_list: list with information about participants
        :param sort_index: need to sort members? 1 - YES, 0 - NO

        :type sort_index: int
        :type students_list: list
        """

        up_row = []
        if self.empty():
            up_row = self.__add_start([])  # default data

        if not students_list:
            print("list is empty")
            return

        if up_row:
            # if the table is empty we just push lst_stud
            # otherwise read table and complement it with lst_stud
            rows = [up_row[:]]
            for i in range(len(students_list)):
                row = [i + 1]
                row.extend(students_list[i])
                rows.append(row[:])
            self.Sheet.append_rows(rows)
            self.res_sum()
        else:
            # if there are already participants in the table,
            # then the method reads their data and adds it to the list with the data we wanted to add,
            # sorting this list, we clear the table and add all the data at once

            rows = self.Sheet.get_all_values()
            start = rows.pop(0)
            rows.pop()

            for number_row in range(len(rows)):
                rows[number_row].pop(0)
                if len(rows[number_row]) >= 5:
                    for number_col in range(5, len(rows[number_row])):
                        if rows[number_row][number_col] != '':
                            rows[number_row][number_col] = float(rows[number_row][number_col].replace(',', '.'))
                else:
                    continue

            # check for duplicate participants
            check = False
            for i in range(0, len(students_list)):
                for j in range(len(rows)):
                    if str(students_list[i][0]) == str(rows[j][0])\
                            and str(students_list[i][1]) == str(rows[j][1]):
                        check = True
                        break
                if not check:
                    rows.append(students_list[i])
                check = False

            # sort list
            rows = sort_participants_name(rows, sort_index)

            temp_table = [start]

            for i in range(1, len(rows) + 1):
                temp_table.append([i])
                temp_table[i].extend(rows[i - 1])
            rows = temp_table

            # cell_range = "A1:" + chr(64 + len(self.Sheet.row_values(1))) + str(len(rows) + 1)
            cell_range = self.create_range(1, 1, len(start), len(rows))

            self.Sheet.clear()
            self.Sheet.append_rows(rows, table_range=cell_range)
            self.res_sum()

    @staticmethod
    def creating_str_date(start_day, last_day, month, year):
        """
        The method generates a list of dates by start_day, last_day, month, year

        :param start_day: day from which to start generating dates
        :param last_day: day until which we generate dates
        :param month: month in which the date is
        :param year: year in which the date is

        :type start_day: int
        :type last_day: int
        :type month: int
        :type year: int

        :return: list of generated dates
        :rtype: list
        """
        dates_list = []

        for i in range(int(start_day), int(last_day)):
            if i < 10:
                if month < 10:
                    dates_list.append("0" + str(i) + ".0" + str(month) + "." + str(year))
                else:
                    dates_list.append("0" + str(i) + "." + str(month) + "." + str(year))
            else:
                if month < 10:
                    dates_list.append(str(i) + ".0" + str(month) + "." + str(year))
                else:
                    dates_list.append(str(i) + "." + str(month) + "." + str(year))

        return dates_list


def debug_google_sheet_clear():
    """
    Method clears all tables from data
    """
    with open(Path.cwd() / 'data_files' / 'clubList.json') as file:
        universities = json.load(file)
    for key in universities.keys():
        sheet_name = universities[key]['googleSheet']

        student_sheet = GoogleSheetsFeature(sheet_name)
        print(f'\n{sheet_name}\n')
        student_sheet.Sheet.clear()
        time.sleep(16)


def check_tess_stud(inf_stud):
    """
    Method checks student card

    :param inf_stud: list with students

    :type inf_stud: list

    :return checklist
    :rtype list
    """
    for i in range(len(inf_stud)):
        inf_stud[i].extend([''])

    return inf_stud


# def working_space(students_info):
#     """
#     This function is a program interface for interaction with the program
#
#     :param students_info: dict in format
#      {'university_name': [[athlete_name, social_net, student_doc, strava_nickname], ...]}
#     :type students_info: dict
#
#     :return: 0 - everything is fine,
#              -1 - error happend
#     :rtype: int
#     """
#     error_indicator = 0
#
#     # debug_google_sheet_clear()
#
#     with open(Path.cwd() / 'data_files' / 'clubList.json') as file:
#         universities = json.load(file)
#
#     load_dotenv()
#     strava_session = StravaNicknames(os.getenv('LOGIN'), os.getenv('PASSWORD'))
#
#     for key in universities.keys():
#         sheet_name = universities[key]['googleSheet']
#
#         if not students_info[sheet_name]:
#             continue
#
#         start_time = time.time()
#
#         for index, nick in enumerate(students_info[sheet_name]):
#             profile_url = nick[3]
#             print(profile_url)
#             nick_from_url = strava_session.get_strava_nickname_from_url(profile_url)
#
#             if profile_url != nick_from_url:
#                 students_info[sheet_name][index][3] = nick_from_url
#
#         students_info[sheet_name] = check_tess_stud(students_info[sheet_name], sheet_name)[:]
#
#         if time.time() - start_time > 17:
#             delay = 0
#         else:
#             delay = 17 - round(time.time() - start_time) + 2
#
#         time.sleep(delay)
#
#         try:
#             student_sheet = GoogleSheetsFeature(sheet_name)
#             student_sheet.add_student(students_info[sheet_name])
#         except gspread.exceptions.APIError:
#             error_indicator = 1
#             print('Ok, here we go again...')
#             time.sleep(100)
#             with open(Path.cwd() / 'data_files' / 'failed_sheets.txt', 'a') as file_update:
#                 file_update.write(str(datetime.datetime.now()) + '   ' + sheet_name + '\n')
#
#         # print(students_info[sheet_name])
#         print(f'\n{sheet_name}\n')
#
#     return error_indicator
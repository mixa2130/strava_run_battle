import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pathlib import Path


class GoogleSheet:
    """
    The Class deals with connecting and interacting with the google sheet
    """
    def __init__(self, google_sheet_name):
        """
        :param google_sheet_name: name of the sheet

        :type google_sheet_name: str
        """
        self.name_Sheet = google_sheet_name
        self.Sheet = self.__authorization()

    def __authorization(self):
        """
        This method is used to connect to the table

        :return: returns an authorized table in google sheet
        """
        scope = ["https://spreadsheets.google.com/feeds", 'https://www.googleapis.com/auth/spreadsheets',
                 "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive"]
        creds = ServiceAccountCredentials.from_json_keyfile_name(Path.cwd() / 'data_files' / 'googleSheet.json',
                                                                 scope)
        client = gspread.authorize(creds)
        sheet = client.open(self.name_Sheet).sheet1
        return sheet

    def __get_col_index(self, name_cell):
        """
        Method finds the column index by name its name

        :param name_cell: name col

        :type name_cell: str

        :return: number col
        :rtype: int
        """

        if name_cell in self.Sheet.row_values(1):
            return self.Sheet.row_values(1).index(name_cell) + 1
        else:
            # print("column not found")
            return -1

    @staticmethod
    def create_range(letter_row, row_range, letter_col, col_range):
        """
        Method created pushing range in table

        :param letter_row: first letter of range
        :param row_range: first numeral of range
        :param letter_col: second letter of range
        :param col_range: second numeral of range

        :type letter_row: int
        :type row_range: int
        :type letter_col: int
        :type col_range: int

        :return: string table range
        :rtype: str
        """
        if letter_row <= 0 or letter_col <= 0 or row_range < 0 or col_range < 0:
            print('error range')
            exit()

        cell_range = ''
        if letter_row + 64 <= 90 and letter_col + 64 <= 90:

            cell_range = chr(64 + letter_row) + str(row_range) + ":" + chr(64 + letter_col) + str(col_range + 1)
        elif letter_row + 64 >= 90:
            cell_range = chr(65) + chr((64 + letter_row - 90) + 64) + str(row_range) + ":" + chr(65) + \
                         chr((64 + letter_col - 90) + 64) + str(col_range + 1)
        elif letter_col + 64 >= 90:
            cell_range = chr(64 + letter_row) + str(row_range) + ":" + chr(65) + \
                         chr((64 + letter_col - 90) + 64) + str(col_range + 1)

        return cell_range

    def get_data(self, name_col):
        """
        The method returns a list of values of a name_col column

        :param name_col: the name of the column to find

        :type name_col: str

        :return: list of column values
        :rtype: list
        """

        index = self.__get_col_index(name_col)
        if index != -1:
            lst = self.Sheet.col_values(index)
            lst.remove(name_col)
            return lst
        else:
            return []

    def add_data(self, name_col, data_list):
        """
        The method is used to add lst in str_col column
        the data is presented in the form of a list and is pushed by the column to the str_column)

        :param name_col: the name of the column into which we will insert
        :param data_list: list of values to insert

        :type name_col: str
        :type data_list: list
        """
        index = self.__get_col_index(name_col)
        if index == -1:
            self.add_col(name_col)
            index = len(self.Sheet.row_values(1))

        # cell_range = chr(64 + index) + "2" + ":" + chr(64 + index) + str(len(lst) + 1)
        cell_range = self.create_range(index, 2, index, len(data_list))

        cell_list = self.Sheet.range(cell_range)  # pushing range

        for i in range(0, len(cell_list)):
            cell_list[i].value = data_list[i]

        self.Sheet.update_cells(cell_list)
        self.res_sum()

    def res_sum(self):
        """
        The method creates formulas for calculating the sum of the columns
        with the results of the race and writes the list
        of formulas to the last row result sum
        """
        num_row = self.Sheet.row_values(1)
        num_col = self.Sheet.col_values(1)

        if len(num_col[len(num_col) - 1]) == 11:  # 11 - len 'result sum:'
            table_range = self.create_range(1, len(num_col), len(num_row), len(num_col) - 1)
            # table_range = 'A' + str(len(num_col)) + ':' + chr(64 + len(num_row)) + str(len(num_col))
            num_col.pop()  # delete 'result sum:'

        else:
            # table_range = 'A' + str(len(num_col) + 1) + ':' + chr(64 + len(num_row)) + str(len(num_col) + 1)
            table_range = self.create_range(1, len(num_col) + 1, len(num_row), len(num_col))

        sum_lst = ['result sum:', '', '', '', '', '']

        for i in range(7, len(num_row) + 1):
            #  sum_range = chr(64 + i) + "2:" + chr(64 + i) + str(len(num_col))
            sum_range = self.create_range(i, 2, i, len(num_col) - 1)
            sum_lst.append('=СУММ(' + sum_range + ' )')

        cell_list = self.Sheet.range(table_range)  # pushing range
        for i in range(0, len(cell_list)):
            cell_list[i].value = sum_lst[i]
        self.Sheet.update_cells(cell_list, value_input_option='USER_ENTERED')

    def get_dates(self):
        """
        Method returns a list of dates

        :return: list dates
        :rtype: list
        """
        temp_list = self.Sheet.row_values(1)
        dates_list = []
        for i in range(6, len(temp_list)):
            dates_list.append(temp_list[i])
        return dates_list

    def clear(self):
        """
        Method completely clears the table of values
        """
        self.Sheet.clear()

    def add_col(self, col):
        """
        Method adds the column name if there is no such column
        :param col: name col

        :type col: str

        :return: if there is a column, exit with the method
        """
        temp_list = self.Sheet.row_values(1)
        for name_col in temp_list:
            if str(name_col) == str(col):
                return

        self.Sheet.update_cell(1, len(temp_list) + 1, col)

    def empty(self):
        """
        The method checks the table for emptiness and correctness of the table
        :return: is the table empty
        :rtype: bool
        """
        if not self.Sheet.row_values(1):
            return 1
        else:
            return 0
